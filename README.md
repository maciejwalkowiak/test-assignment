# Number 26 Test Assignment

In-memory implementation of **Transaction Service** from Number 26 test assignment.  

## Used Technologies

- Spring Boot
- Maven
- Java 8
- Jackson for JSON serialization

## How To Build

`./mvnw clean package`

## How To Run

`java -jar n26-0.0.1-SNAPSHOT.jar`

## Asymptotic complexity

It has been a while since I was thinking in terms of asymptotic notation and today it is definitely not my strong skill.

Service is optimized for writing - all saves to in memory store are done in constant time - `O(1)` (as it is guaranteed by `ConcurrentHashMap`). 
Same applies to getting transaction. 
On the other hand, calculating sum it's the slowest part of the system - it requires traversing database in search for parent transactions, sums are calculated on the fly. 
The complexity of getting transaction sum is `O(n)` (or worse).

If performance of calculating sum would be a concern, there are few options:

- optimize system for reading, either by:
  - sacrificing `InMemoryTransactionRepository` code readability
  - implementing caching
  - sacrificing write performance
  - persisting transactions in a background thread - it would mean that database is eventually consistent
- separating data models into separate model for writing and reading (CQRS way) - which would increase overall complexity