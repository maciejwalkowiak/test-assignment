package com.maciejwalkowiak.n26.domain;

import org.junit.Test;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

/**
 * Tests for {@link TransactionService}
 */
public class TransactionServiceTest {

    private TransactionRepository transactionRepository = mock(TransactionRepository.class);

    private TransactionService transactionService = new TransactionService(transactionRepository);

    @Test
    public void shouldCalculateSumForSingleTransaction() {
        Transaction transaction = new Transaction(1L, 2000d, "car");

        given(transactionRepository.findOne(1L)).willReturn(Optional.of(transaction));
        given(transactionRepository.findByParentId(1L)).willReturn(Collections.emptyList());

        assertThat(transactionService.sum(1L)).isEqualTo(Optional.of(2000d));
    }

    @Test
    public void shouldCalculateSumForNestedTransactions() {
        Transaction transaction = new Transaction(1L, 2000d, "car");
        Transaction transaction2 = new Transaction(2L, 3000d, "car", 1L);

        given(transactionRepository.findOne(1L)).willReturn(Optional.of(transaction));
        given(transactionRepository.findByParentId(1L)).willReturn(Collections.singletonList(transaction2));

        given(transactionRepository.findOne(2L)).willReturn(Optional.of(transaction2));
        given(transactionRepository.findByParentId(2L)).willReturn(Collections.emptyList());

        assertThat(transactionService.sum(1L)).isEqualTo(Optional.of(2000d + 3000d));
        assertThat(transactionService.sum(2L)).isEqualTo(Optional.of(3000d));
    }

}