package com.maciejwalkowiak.n26.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for {@link Transaction}
 */
public class TransactionTest {

    @Test
    public void shouldHaveParentId() {
        Transaction transaction = new Transaction(1L, 2000d, "type", 2L);

        assertThat(transaction.getParentId()).hasValue(2L);
    }

    @Test
    public void shouldNotHaveParentId() {
        Transaction transaction = new Transaction(1L, 2000d, "type");

        assertThat(transaction.getParentId()).isEmpty();
    }

}