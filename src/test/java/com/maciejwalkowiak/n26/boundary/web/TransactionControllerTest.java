package com.maciejwalkowiak.n26.boundary.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.maciejwalkowiak.n26.domain.Transaction;
import com.maciejwalkowiak.n26.domain.TransactionAlreadyExistsException;
import com.maciejwalkowiak.n26.domain.TransactionRepository;
import com.maciejwalkowiak.n26.domain.TransactionService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for {@link TransactionController}
 */
@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
public class TransactionControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TransactionService transactionService;

    @MockBean
    private TransactionRepository transactionRepository;

    @Test
    public void getTransactionShouldReturnNoContentWhenTransactionNotFound() throws Exception {
        given(transactionRepository.findOne(1L)).willReturn(Optional.empty());

        this.mvc.perform(get("/transaction/1"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void getTransactionShouldReturnJson() throws Exception {
        given(transactionRepository.findOne(1L)).willReturn(Optional.of(new Transaction(1L, 2000d, "car", 2L)));

        this.mvc.perform(get("/transaction/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.amount", is(2000d)))
                .andExpect(jsonPath("$.type", is("car")))
                .andExpect(jsonPath("$.parent_id", is(2)));
    }

    @Test
    public void putTransactionShouldReturnJson() throws Exception {
        Transaction transaction = new Transaction(1L, 2000d, "car", 2L);
        given(transactionRepository.save(Mockito.any())).willReturn(transaction);

        TransactionDto dto = new TransactionDto(transaction.getAmount(), transaction.getType(), transaction.getParentId());

        this.mvc.perform(put("/transaction/1").content(this.json(dto))
                                              .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.amount", is(2000d)))
                .andExpect(jsonPath("$.type", is("car")))
                .andExpect(jsonPath("$.parent_id", is(2)));
    }

    @Test
    public void putTransactionShouldReturnUnprocessableEntityWhenTransactionAlreadyExists() throws Exception {
        TransactionDto dto = new TransactionDto(2000d, "car", Optional.of(2L));
        given(transactionRepository.save(Mockito.any())).willThrow(new TransactionAlreadyExistsException(1L));

        this.mvc.perform(put("/transaction/1").content(this.json(dto))
                                              .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void putTransactionShouldReturnBadRequestWhenTransactionIsInvalid() throws Exception {
        TransactionDto dto = new TransactionDto(-2000d, "car", Optional.of(2L));

        this.mvc.perform(put("/transaction/1").content(this.json(dto))
                                              .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void findByTypeShouldReturnListOfIds() throws Exception {
        given(transactionRepository.findByType("car")).willReturn(Arrays.asList(new Transaction(1L, 2000d, "car", 2L),
                                                                             new Transaction(2L, 2000d, "car", 2L)));

        this.mvc.perform(get("/types/car"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(Arrays.asList(1, 2))));
    }

    @Test
    public void sumShouldReturnNoContentWhenTransactionNotFound() throws Exception {
        given(transactionService.sum(1L)).willReturn(Optional.empty());

        this.mvc.perform(get("/sum/1"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void sumShouldReturnJson() throws Exception {
        given(transactionService.sum(1L)).willReturn(Optional.of(20d));

        this.mvc.perform(get("/sum/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sum", is(20d)));
    }

    private String json(Object o) throws JsonProcessingException {
        return objectMapper.writeValueAsString(o);
    }
}