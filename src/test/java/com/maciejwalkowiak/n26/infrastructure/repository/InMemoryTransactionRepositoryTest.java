package com.maciejwalkowiak.n26.infrastructure.repository;

import com.maciejwalkowiak.n26.domain.Transaction;
import com.maciejwalkowiak.n26.domain.TransactionAlreadyExistsException;

import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Tests for {@link InMemoryTransactionRepository}
 */
public class InMemoryTransactionRepositoryTest {

    private InMemoryTransactionRepository transactionRepository = new InMemoryTransactionRepository();

    @Test
    public void shouldSaveTransaction() {
        Transaction transaction = new Transaction(1L, 2000, "car");

        transactionRepository.save(transaction);

        assertThat(transactionRepository.findOne(1L)).isEqualTo(Optional.of(transaction));
    }

    @Test
    public void shouldReturnEmptyWhenTransactionNotFound() {
        assertThat(transactionRepository.findOne(1L)).isEmpty();
    }

    @Test
    public void shouldThrowExceptionWhenTransactionWithGivenIdAlreadyExists() {
        Transaction transaction = new Transaction(1L, 2000, "car");
        transactionRepository.save(transaction);

        Transaction newTransaction = new Transaction(1L, 5000, "car");

        assertThatThrownBy(() -> transactionRepository.save(newTransaction)).isInstanceOf(TransactionAlreadyExistsException.class);
    }

    @Test
    public void shouldFindByType() {
        Transaction carTransaction = new Transaction(1L, 2000d, "car");
        Transaction planeTransaction = new Transaction(2L, 2000d, "plane");
        transactionRepository.save(carTransaction);
        transactionRepository.save(planeTransaction);

        assertThat(transactionRepository.findByType("car")).containsExactly(carTransaction);
        assertThat(transactionRepository.findByType("plane")).containsExactly(planeTransaction);
    }

    @Test
    public void shouldFindByParentId() {
        Transaction transaction = new Transaction(1L, 2000d, "car");
        Transaction childTransaction = new Transaction(2L, 2000d, "plane", 1L);

        transactionRepository.save(transaction);
        transactionRepository.save(childTransaction);

        assertThat(transactionRepository.findByParentId(1L)).containsExactly(childTransaction);
    }
}