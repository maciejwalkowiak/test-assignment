package com.maciejwalkowiak.n26.domain;

import java.util.List;
import java.util.Optional;

public interface TransactionRepository {
    /**
     * Finds transaction in data store.
     *
     * @param id - transaction id
     * @return transaction or {@link Optional#EMPTY} if not found.
     */
    Optional<Transaction> findOne(Long id);

    /**
     * Saves transaction in data store.
     *
     * @param transaction - transaction to save
     * @return saved transaction
     * @throws TransactionAlreadyExistsException if transaction with given id already exists
     */
    Transaction save(Transaction transaction);

    /**
     * Finds transactions by type.
     *
     * @param type - type of transaction
     * @return list of transactions or empty list if not found
     */
    List<Transaction> findByType(String type);

    /**
     * Finds transactions by parent id.
     *
     * @param id - parent transaction id
     * @return list of transactions or empty list if not found
     */
    List<Transaction> findByParentId(Long id);
}
