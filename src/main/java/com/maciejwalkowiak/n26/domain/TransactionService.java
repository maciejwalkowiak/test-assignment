package com.maciejwalkowiak.n26.domain;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;

    public TransactionService(TransactionRepository transactionRepository) {
        Assert.notNull(transactionRepository, "transactionRepository cannot be null");
        this.transactionRepository = transactionRepository;
    }

    /**
     * Calculates sum of transaction's and its children amounts.
     *
     * @param id - transaction id
     * @return sum or {@link Optional#EMPTY} if transaction not found
     */
    public Optional<Double> sum(Long id) {
        Assert.notNull(id, "id cannot be null");
        return transactionRepository.findOne(id).map(this::sumWithChildren);
    }

    private double sumWithChildren(Transaction parent) {
        return parent.getAmount() + transactionRepository.findByParentId(parent.getId())
                                                         .stream()
                                                         .mapToDouble(this::sumWithChildren)
                                                         .sum();
    }
}
