package com.maciejwalkowiak.n26.domain;

public class TransactionAlreadyExistsException extends RuntimeException {

    public TransactionAlreadyExistsException(Long id) {
        super("Transaction with [id=" + id + "] already exists");
    }
}
