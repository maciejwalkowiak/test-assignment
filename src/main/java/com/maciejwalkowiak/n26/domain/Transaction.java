package com.maciejwalkowiak.n26.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.util.Assert;

import java.util.Optional;

public class Transaction {
    private final Long id;
    private final double amount;
    private final String type;
    private final Optional<Long> parentId;

    public Transaction(Long id, double amount, String type, Optional<Long> parentId) {
        Assert.notNull(id, "id cannot be null");
        Assert.notNull(type, "type cannot be null");
        Assert.notNull(parentId, "parentId cannot be null");
        Assert.isTrue(amount >= 0, "amount has to be greater or equal 0");
        this.id = id;
        this.amount = amount;
        this.type = type;
        this.parentId = parentId;
    }

    public Transaction(Long id, double amount, String type) {
        this(id, amount, type, Optional.empty());
    }

    public Transaction(Long id, double amount, String type, Long parentId) {
        this(id, amount, type, Optional.of(parentId));
    }

    public boolean hasParent(Long parentId) {
        return this.parentId.map(parentId::equals)
                            .orElse(false);
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public double getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public Optional<Long> getParentId() {
        return parentId;
    }
}
