package com.maciejwalkowiak.n26.infrastructure.repository;

import com.maciejwalkowiak.n26.domain.Transaction;
import com.maciejwalkowiak.n26.domain.TransactionAlreadyExistsException;
import com.maciejwalkowiak.n26.domain.TransactionRepository;

import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Repository
class InMemoryTransactionRepository implements TransactionRepository {

    private final Map<Long, Transaction> transactions = new ConcurrentHashMap<>();

    @Override
    public Optional<Transaction> findOne(Long id) {
        Assert.notNull(id, "id cannot be null");
        return Optional.ofNullable(transactions.get(id));
    }

    @Override
    public Transaction save(Transaction transaction) {
        Assert.notNull(transaction, "transaction cannot be null");

        Transaction previousValue = transactions.putIfAbsent(transaction.getId(), transaction);

        if (previousValue != null) {
            throw new TransactionAlreadyExistsException(transaction.getId());
        }

        return transaction;
    }

    @Override
    public List<Transaction> findByType(String type) {
        Assert.notNull(type, "type cannot be null");
        return transactions.values()
                           .stream()
                           .filter(transaction -> transaction.getType().equals(type))
                           .collect(Collectors.toList());
    }

    @Override
    public List<Transaction> findByParentId(Long id) {
        Assert.notNull(id, "id cannot be null");
        return transactions.values()
                           .stream()
                           .filter(transaction -> transaction.hasParent(id))
                           .collect(Collectors.toList());
    }
}
