package com.maciejwalkowiak.n26.boundary.web;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Optional;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

class TransactionDto {

    @Min(0)
    private final double amount;
    @NotNull
    private final String type;
    private final Optional<Long> parentId;

    public TransactionDto(@JsonProperty("amount") double amount,
                          @JsonProperty("type") String type,
                          @JsonProperty("parent_id") Optional<Long> parentId) {
        this.amount = amount;
        this.type = type;
        this.parentId = parentId;
    }

    public double getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public Optional<Long> getParentId() {
        return parentId;
    }
}
