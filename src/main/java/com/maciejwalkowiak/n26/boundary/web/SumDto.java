package com.maciejwalkowiak.n26.boundary.web;

class SumDto {

    private final double sum;

    public SumDto(double sum) {
        this.sum = sum;
    }

    public double getSum() {
        return sum;
    }
}
