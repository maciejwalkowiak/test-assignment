package com.maciejwalkowiak.n26.boundary.web;

import com.maciejwalkowiak.n26.domain.Transaction;
import com.maciejwalkowiak.n26.domain.TransactionAlreadyExistsException;
import com.maciejwalkowiak.n26.domain.TransactionRepository;
import com.maciejwalkowiak.n26.domain.TransactionService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

@RestController
class TransactionController {

    private final TransactionService transactionService;
    private final TransactionRepository transactionRepository;

    public TransactionController(TransactionService transactionService, TransactionRepository transactionRepository) {
        Assert.notNull(transactionService, "transactionService cannot be null");
        Assert.notNull(transactionRepository, "transactionRepository cannot be null");
        this.transactionService = transactionService;
        this.transactionRepository = transactionRepository;
    }

    @PutMapping("/transaction/{transactionId}")
    Transaction putTransaction(@PathVariable Long transactionId, @RequestBody @Valid TransactionDto dto) {
        return transactionRepository.save(new Transaction(transactionId, dto.getAmount(), dto.getType(), dto.getParentId()));
    }

    @GetMapping("/transaction/{transactionId}")
    ResponseEntity<Transaction> getTransaction(@PathVariable Long transactionId) {
        return transactionRepository.findOne(transactionId)
                                    .map(ResponseEntity::ok)
                                    .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping("/types/{type}")
    List<Long> findByType(@PathVariable String type) {
        return transactionRepository.findByType(type)
                                    .stream()
                                    .map(Transaction::getId)
                                    .collect(Collectors.toList());
    }

    @GetMapping("/sum/{transactionId}")
    ResponseEntity<SumDto> sum(@PathVariable Long transactionId) {
        return transactionService.sum(transactionId)
                                 .map(SumDto::new)
                                 .map(ResponseEntity::ok)
                                 .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @ExceptionHandler(TransactionAlreadyExistsException.class)
    ResponseEntity<Void> handle(TransactionAlreadyExistsException e) {
        return ResponseEntity.unprocessableEntity().build();
    }
}
